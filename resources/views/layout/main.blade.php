<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  	<head>

	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="csrf-token" content="{{ csrf_token() }}">

	    @include('layout.styles')

	    <title>CashStash</title>
  	</head>
  	<body>

  		@include('layout.nav')

  		<div id="app">

	  		@yield('content')

    	</div>

		@include('layout.scripts')

  	</body>
</html>