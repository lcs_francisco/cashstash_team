@extends('layout.main')

@section('content')

	<section class="margined-top text-center">
		<chart 
			title="Percentiles" 
			chart_id="percentiles" 
			type="pie" 
			main_chart="Current Month" 
			comparison_chart="Last Month">
		</chart>
		<chart 
			title="Income" 
			chart_id="income" 
			type="bar" 
			main_chart="Current Month" 
			comparison_chart="Last Month">
		</chart>
		<chart 
			title="Expenses" 
			chart_id="expenses" 
			type="line" 
			main_chart="Current Month" 
			comparison_chart="Last Month">
		</chart>
		<chart 
			title="Investments" 
			chart_id="investments" 
			type="line" 
			main_chart="Current Month" 
			comparison_chart="Last Month">
		</chart>
		<chart 
			title="Savings" 
			chart_id="savings" 
			type="bar" 
			main_chart="Current Month" 
			comparison_chart="Last Month">
		</chart>
	</section>

@stop